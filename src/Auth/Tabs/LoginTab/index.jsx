import React, { Fragment, useState, useEffect, useContext } from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { Btn, H4, P } from '../../../AbstractElements';
import { EmailAddress, ForgotPassword, LoginWithJWT, Password, RememberPassword, SignIn } from '../../../Constant';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { firebase_app, Jwt_token } from '../../../Config/Config';
import man from '../../../assets/images/dashboard-2/1.png';
import { handleResponse } from '../../../Services/fack.backend';
import SocialAuth from './SocialAuth';
import CustomizerContext from '../../../_helper/Customizer';

const LoginTab = ({ selected }) => {
  const [email, setEmail] = useState('test@gmail.com');
  const [password, setPassword] = useState('test123');
  const [loading, setLoading] = useState(false);
  const [togglePassword, setTogglePassword] = useState(false);
  const history = useNavigate();
  const { layoutURL } = useContext(CustomizerContext);

  const [value, setValue] = useState(localStorage.getItem('profileURL' || man));
  const [name, setName] = useState(localStorage.getItem('Name'));

  useEffect(() => {
    localStorage.setItem('profileURL', value);
    localStorage.setItem('Name', name);
  }, [value, name]);

  const register = () => {
    window.location.href = `${process.env.PUBLIC_URL}/pages/register/`;
  }

  const loginWithJwt = (e) => {
    let role = ""
    if (email === 'admin@gmail.com') {
      role = "admin"
    }
    if (email === 'user1@gmail.com') {
      role = "user"
    }
    if (email === 'officer1@gmail.com') {
      role = "officer"
    }
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: { email, password },
    };
    return fetch('/users/authenticate', requestOptions)
      .then(handleResponse)
      .then(async (user) => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        setValue(man);
        setName(email);
        await localStorage.setItem('token', Jwt_token);
        await localStorage.setItem('role', role);
        // if admin
        // window.location.href = `${process.env.PUBLIC_URL}/app/dashboard`;
        // if user
          window.location.href = `${process.env.PUBLIC_URL}/dashboard`;
          return user;

      });
  };

  return (
    <Fragment>
      <Form className='theme-form'>
        <H4>E-Compound Checker</H4>
        <P>{'Enter your email & password to login'}</P>
        <FormGroup>
          <Label className='col-form-label'>{EmailAddress}</Label>
          <Input className='form-control' type='email' required='' onChange={(e) => setEmail(e.target.value)} defaultValue={email} />
        </FormGroup>
        <FormGroup className='position-relative'>
          <Label className='col-form-label'>{Password}</Label>
          <Input className='form-control' type={togglePassword ? 'text' : 'password'} onChange={(e) => setPassword(e.target.value)} defaultValue={password} required='' />
          <div className='show-hide' onClick={() => setTogglePassword(!togglePassword)}>
            <span className={togglePassword ? '' : 'show'}></span>
          </div>
        </FormGroup>
        <div className='position-relative form-group mb-0'>
          {/* <div className='checkbox'>
            <Input id='checkbox1' type='checkbox' />
            <Label className='text-muted' for='checkbox1'>
              {RememberPassword}
            </Label>
          </div>
          <a className='link' href='#javascript'>
            {ForgotPassword}
          </a> */}
          <Btn attrBtn={{ color: 'primary', className: 'd-block w-100 mt-2', disabled: loading ? loading : loading, onClick: (e) => loginWithJwt(e) }}>{loading ? 'LOADING...' : LoginWithJWT}</Btn>
          <Btn attrBtn={{ color: 'secondary', className: 'd-block w-100 mt-2', disabled: loading ? loading : loading, onClick: (e) => register() }}>{loading ? 'LOADING...' : 'Register'}</Btn>
        </div>
      </Form>
    </Fragment>
  );
};

export default LoginTab;
