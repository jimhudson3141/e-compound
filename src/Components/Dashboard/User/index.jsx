import React, { Fragment } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Breadcrumbs } from '../../../AbstractElements'

const UserDashboard = () => {
    return (
        <Fragment>
            <Breadcrumbs mainTitle="User" parent="Dashboard" title="User" />
            <Container fluid={true} >
                <Row className="size-column">
                    <Col xl="7 xl-100" className="box-col-12 ">
                        <Row className="dash-chart">
                           
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
};

export default UserDashboard;