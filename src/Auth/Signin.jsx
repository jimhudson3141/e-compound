import React, { useState } from 'react';
import { Container, Row, Col, TabContent, TabPane } from 'reactstrap';
import { Image } from '../AbstractElements';
import NavAuth from './Nav';
import LoginTab from './Tabs/LoginTab';

const Logins = () => {
  const [selected, setSelected] = useState('jwt');

  const callbackNav = ((select) => {
    setSelected(select);
  });

  return (
    <Container fluid={true} className="p-0 login-page">
      <Row>
        <Col xs="12">
          <div className="login-card">
            <div className="login-main login-tab">
              <Image attrImage={{ className: 'rounded-circle img-80 me-3', src: `https://i.imgur.com/onhPW23.jpg`, alt: 'Generic placeholder image' }} />
              <NavAuth callbackNav={callbackNav} selected={selected} />
              <TabContent activeTab={selected} className="content-login">
                <TabPane className="fade show" tabId={'jwt'}>
                  <LoginTab selected={selected} />
                </TabPane>
                {/* <TabPane className="fade show" tabId="auth0">
                  <AuthTab />
                </TabPane> */}
              </TabContent >
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Logins;