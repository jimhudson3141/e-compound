import React, { Fragment } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Breadcrumbs } from '../../../AbstractElements'
import TotalCompound from "./TotalCompound"
import TotalOffender from "./TotalOffender"
import TotalOfficer from "./TotalOfficer"

const AdminDashboard = () => {


    return (
        <Fragment>
            <Breadcrumbs mainTitle="Admin" parent="Dashboard" title="Admin" />
            <Container fluid={true} >
                <Row className="size-column">
                    <Col xl="7 xl-100" className="box-col-12 ">
                        <Row className="dash-chart">
                            <TotalCompound />
                            <TotalOffender />
                            <TotalOfficer />
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
};

export default AdminDashboard;