import React, { Fragment, useContext, useState } from 'react'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom';
import { Col, Card, CardHeader, CardBody, Form, FormGroup, Label, Input, Container, Row, Table, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Breadcrumbs, Btn, H5, H6, P } from '../../AbstractElements'
import TableContext from '../../_helper/Table';

const OfficerCompounds = () => {
    const { data } = useContext(TableContext);
    const [PrimarycolorLineTab, setPrimarycolorLineTab] = useState('1');

    return (
        <Fragment>
            <Breadcrumbs mainTitle="Compound" parent="Officer" title="Compound" />
            <Container fluid={true}>
                <Row>

                    <Col sm="12" xl="6" className='xl-100'>
                        <Card>
                            <CardHeader>
                                <Row>
                                    <Col md="auto">
                                        <H5>{'Add New Type Of Compound'}</H5>
                                    </Col>
                                    <Col md="auto">
                                        <Btn attrBtn={{
                                            color: 'primary', size: 'small', style: { margin: 5 }
                                        }}>New + </Btn>
                                    </Col>
                                </Row>
                            </CardHeader>
                            <CardBody>
                                <Nav className="border-tab nav-primary" tabs>
                                    <NavItem>
                                        <NavLink href="#javascript" className={PrimarycolorLineTab === '1' ? 'active' : ''} onClick={() => setPrimarycolorLineTab('1')}><i className="icofont icofont-bird"></i>{'Dengue'}</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="#javascript" className={PrimarycolorLineTab === '2' ? 'active' : ''} onClick={() => setPrimarycolorLineTab('2')}><i className="icofont icofont-no-smoking"></i>{'Cigarette'}</NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={PrimarycolorLineTab}>
                                    <TabPane className="fade show" tabId="1">
                                        <Table>
                                            <thead>
                                                <tr>
                                                    <th scope="col">{'#'}</th>
                                                    <th scope="col">{'Compound ID'}</th>
                                                    <th scope="col">{'Offences Description'}</th>
                                                    <th scope="col">{'Compound Amount (RM)'}</th>
                                                    <th scope="col">{'Compound Status'}</th>
                                                    <th scope="col">{'Action'}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    data.map((item) =>
                                                        <tr key={item.id}>
                                                            <td>{item.id}</td>
                                                            <td>{item.compound_id}</td>
                                                            <td>{item.offence_desc}</td>
                                                            <td>{item.amount}</td>
                                                            <td>{item.status}</td>
                                                            <td>
                                                                <Btn attrBtn={{
                                                                    color: 'primary', size: 'small', style: { margin: 5 }
                                                                }}>Update Status</Btn>
                                                                <Btn attrBtn={{
                                                                    color: 'info', size: 'small', style: { margin: 5 }
                                                                }}>Edit</Btn>
                                                                <Btn attrBtn={{
                                                                    color: 'secondary', size: 'small', style: { margin: 5 }
                                                                }}>Delete</Btn>
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </Table>
                                    </TabPane>
                                    <TabPane tabId="2">
                                        No data
                                    </TabPane>

                                </TabContent>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    )
}

export default OfficerCompounds
