import React, { Fragment } from 'react'
import DataTable from 'react-data-table-component'
import { Card, CardBody, Col, Container, Row } from 'reactstrap'
import { Breadcrumbs, Btn, H6, Image } from '../../AbstractElements'

const Compounds = () => {
  return (
    <Fragment>
      <Breadcrumbs parent="Admin" title="Compound List" mainTitle="Compound List" />
      <Container fluid={true}>
        <Row>
          <Col sm="12">
            <Card>
              <CardBody>
                <CompoundsTableData />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  )
}

const CompoundsTableData = () => {

  const style = {
    width: 40,
    height: 40
  };
  const style2 = { width: 60, fontSize: 14, padding: 4 };

  const columns = [
    {
      name: 'Image',
      selector: (row) => row.image,
      sortable: true,
      center: true,
    },
    {
      name: 'Name',
      selector: (row) => row.name,
      sortable: true,
      center: true,
      wrap: true
    },
    {
      name: 'Email',
      selector: (row) => row.email,
      sortable: true,
      center: true,
      wrap: true
    },
    {
      name: 'Action',
      selector: (row) => row.action,
      sortable: true,
      center: true,
    },
  ];

  const data = [
    {
      image: <Image attrImage={{ src: 'https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/3_avatar-512.png', style: style, alt: '' }} />,
      name: 'test2',
      email: 'test2@gmail.com',
      action:
        <div>
          <span>
            <Btn attrBtn={{ style: style2, color: 'danger', className: 'btn btn-xs', type: 'button' }}>Delete</Btn>
          </span> &nbsp;&nbsp;
          <span>
            <Btn attrBtn={{ style: style2, color: 'success', className: 'btn btn-xs', type: 'button' }}>Edit </Btn>
          </span>
        </div >
    },
    {
      image: <Image attrImage={{ src: 'https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/3_avatar-512.png', style: style, alt: '' }} />,
      name: 'test1',
      email: 'test1@gmail.com',
      action:
        <div>
          <span>
            <Btn attrBtn={{ style: style2, color: 'danger', className: 'btn btn-xs', type: 'button' }}>Delete</Btn>
          </span> &nbsp;&nbsp;
          <span>
            <Btn attrBtn={{ style: style2, color: 'success', className: 'btn btn-xs', type: 'button' }}>Edit </Btn>
          </span>
        </div >
    },
  ];

  return (
    <Fragment>
      <div className="table-responsive product-table">
        <DataTable
          noHeader
          pagination
          paginationServer
          columns={columns}
          data={data}
        />
      </div>
    </Fragment>
  );
};

export default Compounds
