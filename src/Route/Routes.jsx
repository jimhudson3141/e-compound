// dashbaord
import AdminDashboard from '../Components/Dashboard/Admin';

import Compounds from '../Components/Admin/Compounds';
import Officers from '../Components/Admin/Officers';
import Offenders from '../Components/Admin/Offenders';
import UserCompounds from '../Components/User/Compounds';
import OfficerCompounds from '../Components/Officer/Compounds';


export const routes = [
  //dashboard
  // { path: `${process.env.PUBLIC_URL}/dashboard/default/:layout`, Component: <Default /> },
  { path: `${process.env.PUBLIC_URL}/dashboard`, Component: <AdminDashboard /> },
  // { path: `${process.env.PUBLIC_URL}/dashboard`, Component: <UserDashboard /> },

  // admin 
  { path: `${process.env.PUBLIC_URL}/app/admin/compounds/:layout`, Component: <Compounds /> },
  { path: `${process.env.PUBLIC_URL}/app/admin/officers/:layout`, Component: <Officers /> },
  { path: `${process.env.PUBLIC_URL}/app/admin/offenders/:layout`, Component: <Offenders /> },

  // user 
  { path: `${process.env.PUBLIC_URL}/app/user/compounds/:layout`, Component: <UserCompounds /> },

  // officer 
  { path: `${process.env.PUBLIC_URL}/app/officer/compounds/:layout`, Component: <OfficerCompounds /> },

];
