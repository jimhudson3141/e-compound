import React, { Fragment } from 'react';
import { Facebook, Linkedin, Twitter, Instagram } from 'react-feather';
import { Form, FormGroup, Input, Label, Row, Col } from 'reactstrap';
import { Btn, H4, H6, LI, UL } from '../../../AbstractElements';
import { Link } from 'react-router-dom';

const RegisterFrom = () => {
    return (
        <Fragment>
            <div className="login-card">
                <div className="login-main">
                    <Form className="theme-form login-form">
                        <H4>Create your account</H4>
                        <H6>Enter your personal details to create account</H6>
                        <FormGroup>
                            <Label className="col-form-label">Your Nickname</Label>
                            <Row>
                                <Col xs='12'>
                                    <Input className="form-control" type="text" required="" placeholder="nickname" />
                                </Col>
                            </Row>
                        </FormGroup>
                        <FormGroup>
                            <Label className="col-form-label">Email Address</Label>
                            <Input className="form-control" type="email" required="" placeholder="Test@gmail.com" />
                        </FormGroup>
                        <FormGroup className="position-relative">
                            <Label className="col-form-label">Password</Label>
                            <Input className="form-control" type="password" name="login[password]" required="" placeholder="*********" />
                            <div className="show-hide"><span className="show"> </span></div>
                        </FormGroup>
                        <FormGroup>
                            <Btn attrBtn={{ className: 'd-block w-100', color: 'primary', type: 'submit' }}>Create Account</Btn>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

export default RegisterFrom;