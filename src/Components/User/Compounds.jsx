import React, { Fragment, useContext } from 'react'
import DataTable from 'react-data-table-component'
import { Link } from 'react-router-dom';
import { Col, Card, CardHeader, CardBody, Form, FormGroup, Label, Input, Container, Row, Table } from 'reactstrap';
import { Breadcrumbs, Btn, H5, H6, P } from '../../AbstractElements'
import TableContext from '../../_helper/Table';

const UserCompounds = () => {
    const { data } = useContext(TableContext);

    return (
        <Fragment>
            <Breadcrumbs mainTitle="Compound" parent="User" title="Compound" />
            <Container fluid={true} className="general-widget">
                <Row>
                    <Fragment>
                        <Col sm="12" lg="6" xl="4" md="12" className="xl-50 box-col-6">
                            <Card className="height-equal">
                                <div className="calender-widget">
                                    <div className="cal-img"></div>
                                    <CardBody className="cal-desc text-center">
                                        <H6 attrH6={{ className: 'f-w-600' }} >Notes</H6>
                                        <P attrPara={{ className: 'text-muted mt-3 mb-0' }}>Please clear your summon before any actions taken. Thank you.</P>
                                    </CardBody>
                                </div>
                            </Card>
                        </Col>
                    </Fragment>
                    <Fragment>
                        <Col sm='12' lg='6' xl='8' md='12' className='xl-50 box-col-6'>
                            <Card className='height-equal'>
                                <CardHeader className='d-flex justify-content-between align-items-center'>
                                    <H5>Check Summon</H5>
                                </CardHeader>
                                <CardBody className='contact-form'>
                                    <Form className='theme-form'>
                                        <div className='form-icon'>
                                            <i className='icofont icofont-document-folder'></i>
                                        </div>
                                        <div className='mb-3'>
                                            <Label htmlFor='exampleInputName'>Compound Type</Label>
                                            <Row>
                                                <Col sm="12">
                                                    <div className="radio radio-primary ms-2">
                                                        <Input type="radio" name="radio1" id="radio1" value="option1" />
                                                        <Label for="radio1">{Option} {'Dengue Compound'}</Label>
                                                    </div>
                                                    <div className="radio radio-primary ms-2">
                                                        <Input type="radio" name="radio1" id="radio2" value="option1" />
                                                        <Label for="radio2">{Option} {'Cigarette Compound'}</Label>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div className='mb-3'>
                                            <Label className='col-form-label' htmlFor='exampleInputEmail1'>
                                                Compound ID
                                            </Label>
                                            <Input className='form-control' id='exampleInputEmail1' type='email' placeholder='Type compound ID' />
                                        </div>
                                        <div className='text-sm-end'>
                                            <Link>
                                                <Btn attrBtn={{ as: Card.Header, className: 'btn btn-primary', color: 'default' }}>SEARCH RECORD</Btn>
                                            </Link>
                                        </div>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Fragment>
                </Row>
                <Row>
                    <Fragment>
                        <Col sm="24" lg="24" xl="24" md="24">
                            <Card className="height-equal">
                                <CardHeader>
                                    <H5>{'Records'}</H5>
                                </CardHeader>
                                <div className="table-responsive">
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th scope="col">{'#'}</th>
                                                <th scope="col">{'Compound ID'}</th>
                                                <th scope="col">{'Offences Description'}</th>
                                                <th scope="col">{'Compound Amount (RM)'}</th>
                                                <th scope="col">{'Compound Status'}</th>
                                                <th scope="col">{'Action'}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                data.map((item) =>
                                                    <tr key={item.id}>
                                                        <td>{item.id}</td>
                                                        <td>{item.compound_id}</td>
                                                        <td>{item.offence_desc}</td>
                                                        <td>{item.amount}</td>
                                                        <td>{item.status}</td>
                                                        <td>
                                                            <Btn attrBtn={{
                                                                color: 'secondary', size: 'small',
                                                            }}
                                                            >
                                                               Upload Receipt
                                                            </Btn>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </Table>
                                </div>
                            </Card>
                        </Col>
                    </Fragment>
                </Row>
            </Container>
        </Fragment>
    )
}

export default UserCompounds
